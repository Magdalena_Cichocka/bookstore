import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Homepage from "./pages/Homepage";
import AboutUs from "./pages/About us";
import Bookstores from "./pages/Bookstores";
import Contact from "./pages/Contact";
import FAQ from "./pages/FAQ";
import Cart from "./pages/Cart";
import BookDetails from './pages/Book Details';
import { BooksProvider } from "./components/context";
import "./styles/homepage.css";
import './styles/faq.css';
import './styles/about-us.css';
import './styles/contact-us.css';
import './styles/our-bookstores.css';
import Footer from './components/Footer';
import TopSection from './components/Top Section';

function App() {
  return (
    <BooksProvider>
      <Router>
        <TopSection/><Switch>
          <Route exact path="/">
            <Homepage />
          </Route>
          <Route path='/book/:isbn' children={<BookDetails />}></Route>
          <Route path="/about-us">
            <AboutUs />
          </Route>
          <Route path="/bookstores">
            <Bookstores />
          </Route>
          <Route path="/Contact">
            <Contact />
          </Route>
          <Route path="/faq">
            <FAQ />
          </Route>
          <Route path="/cart">
            <Cart />
          </Route>
        </Switch>
        <Footer/>
      </Router>
    </BooksProvider>
  );
}

export default App;
