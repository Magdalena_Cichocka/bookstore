import { useParams } from "react-router-dom";
import { books } from "../../components/data";
import { Link } from "react-router-dom";
import BookInput from '../../components/Book Input';

const BookDetails = () => {
  const { isbn } = useParams();
  const bookArr = books.filter((el) => el.isbn === parseInt(isbn));
  const book = bookArr[0];

  return (
    <div className="pages__wrapper">
      <div className="book-details">
        <div className="book-details__content">
          <div className="pic">
            <img src={book.cover} alt={`${book.title} cover`}></img>
          </div>
          <div className="text">
            <h2>{book.title}</h2>
            <h3>{book.author}</h3>
            <BookInput el={book}/>
            <p>Year: {book.year_written}</p>
            <p>Number of pages: {book.pages}</p>
            <p>Edition: {book.edition}</p>
            <p>ISBN: {book.isbn}</p>
            <p>Description:</p>
            <p>{book.longDescription}</p>
            <p className='text__categories'>Tags: {book.categories.map((el)=> `${el} `)}</p>
            <div>
              <button className='return-button'>
                <Link to="/">Return</Link>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BookDetails;
