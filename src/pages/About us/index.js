const AboutUs = () => {
  return (
    <div className="pages__wrapper">
      <div className="about-us">
        <div className="about-us__headings">
          <h2>Welcome to the BookStore!</h2>
          <h3>We're happy to see that you're interested in our books.</h3>
        </div>
        <div className="about-us__section">
          <div className="about-us__section__image left">
            <img
              src="https://images.pexels.com/photos/1222463/pexels-photo-1222463.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
              alt="woman with a book"
            ></img>
            <p className="about-us__section__image-caption">
              Please make sure to keep a proper distance from the text when
              reading our books.
            </p>
          </div>
          <div className="about-us__section__text">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
              nisl nisl, luctus eget egestas non, dignissim sit amet nisl.
              Vestibulum sollicitudin dui nec elit lacinia vehicula. Duis
              malesuada quam a libero auctor luctus. Suspendisse ultrices lacus
              nec velit semper gravida porttitor eu lorem. Quisque laoreet
              dictum pharetra. Pellentesque at est quis erat dignissim viverra
              vel id tortor. Nullam elementum tortor eu ante molestie, a commodo
              mauris tristique. Maecenas ultrices tortor et nulla euismod
              cursus. Vivamus ac libero maximus, egestas tellus accumsan,
              condimentum nisl.
            </p>
            <p>
              Morbi sit amet bibendum tellus. Maecenas metus enim, convallis ac
              luctus eu, lacinia nec mi. Donec ut magna egestas, scelerisque leo
              sit amet, pulvinar augue. Sed tincidunt leo at leo viverra
              faucibus. Interdum et malesuada fames ac ante ipsum primis in
              faucibus. Integer et finibus tellus. Phasellus at molestie massa.
              Donec pulvinar sollicitudin elit vel feugiat. Phasellus rutrum
              eros dolor. Quisque tristique nibh sed commodo vulputate.
              Pellentesque lacus est, ullamcorper non euismod in, lacinia eget
              nisl.
            </p>
            <p>
              In tincidunt auctor diam sit amet ultricies. Morbi in pellentesque
              ipsum. Cras cursus neque massa. Sed ultricies purus vel enim
              ullamcorper commodo. Donec metus orci, suscipit ut risus eu,
              auctor ullamcorper metus. Nunc a dignissim libero. Morbi lobortis
              volutpat nisi in cursus. Integer et dui id augue congue maximus.
              Integer at mauris sed nisi placerat lobortis quis vitae quam.
            </p>
            <p>
              Curabitur at est ullamcorper odio aliquet dignissim. Cras libero
              sem, commodo nec varius non, aliquet eu tellus. Duis ex massa,
              tempus nec condimentum ac, gravida eget ex. Proin pretium placerat
              nunc, nec rutrum purus tristique nec. Nulla luctus porta sem ut
              semper. Mauris efficitur nibh quis est blandit sodales. Proin
              molestie magna augue, eu placerat justo posuere accumsan. Nulla
              gravida, nisi et laoreet mollis, sapien ante dictum massa, sed
              eleifend enim tellus sit amet sem.
            </p>
          </div>
          <div className="about-us__section__image right">
            <img
              src="https://images.pexels.com/photos/3808243/pexels-photo-3808243.jpeg?cs=srgb&dl=pexels-dina-nasyrova-3808243.jpg&fm=jpg"
              alt="a cup of coffee standing close to the book"
            ></img>
            <p className="about-us__section__image-caption">
              If you spill a drink on your book, you might need a new one. We
              don't mind.
            </p>
          </div>
          <div className="about-us__section__text">
            <p>
              Ut ac congue purus. Praesent erat urna, maximus in lectus quis,
              tincidunt blandit nulla. Nullam ante nunc, laoreet ac odio in,
              molestie pulvinar eros. Cras mollis, est ut dictum condimentum,
              ipsum est rhoncus dolor, malesuada finibus urna magna vel neque.
              Morbi faucibus erat mauris. Nullam lectus nisl, accumsan vitae
              tortor eu, hendrerit pretium arcu. Integer nec ligula ac massa
              semper feugiat eu eu justo. Nulla in mi non ligula laoreet
              ultrices eu vel arcu. Integer auctor, tortor sit amet vestibulum
              pulvinar, neque sem semper odio, sagittis finibus dolor augue
              dictum ligula. Aliquam sit amet imperdiet est. Vestibulum
              fringilla, felis et dignissim iaculis, nulla metus vestibulum
              dolor, non lacinia erat magna ac lacus. Sed eget sapien ut libero
              consequat pharetra. Maecenas at dui fermentum, sagittis nisl sit
              amet, pellentesque mauris. Nunc laoreet consequat ornare. Vivamus
              massa enim, vulputate id tincidunt a, faucibus ut ligula. Sed
              placerat lacus posuere faucibus tincidunt.
            </p>
            <p>
              Nunc consectetur molestie nibh, vel pellentesque lectus porttitor
              consectetur. Pellentesque habitant morbi tristique senectus et
              netus et malesuada fames ac turpis egestas. Quisque rutrum, turpis
              eu rutrum eleifend, libero erat auctor ante, quis mollis dolor est
              vel enim. Sed ultricies elit in lorem egestas, quis aliquam ante
              volutpat. Phasellus gravida purus id nulla condimentum fringilla
              vel sed dui. Ut viverra ipsum eget viverra varius. Donec efficitur
              mattis tempus. Vivamus sed sagittis odio. Integer consectetur
              sapien erat, vel porta turpis faucibus sit amet. Donec sed pretium
              dui. Morbi at cursus justo, ac volutpat mauris. Fusce et est
              sollicitudin mauris iaculis sodales iaculis sit amet ligula.
              Aliquam vitae fringilla orci, vel vulputate justo. Duis faucibus
              nec diam ut faucibus.
            </p>
          </div>
          <div className="about-us__section__image left">
            <img
              src="https://images.pexels.com/photos/4156286/pexels-photo-4156286.jpeg?cs=srgb&dl=pexels-cottonbro-4156286.jpg&fm=jpg"
              alt="a person reading a book in a bathtub close to candles"
            ></img>
            <p className="about-us__section__image-caption">
              If coffee isn't destructive enough, you may try to drown the book. Burn it.
              Whatever. Just make sure to buy another one in our BookStore!
            </p>
          </div>
          <div className="about-us__section__text">
            <p>
              Phasellus malesuada tempus faucibus. Duis iaculis arcu in urna
              dictum congue. Integer dapibus tellus id auctor auctor. Aliquam ut
              varius dolor. Quisque consequat malesuada nulla, eget semper purus
              aliquet ac. Praesent at lacus ipsum. Vivamus eget sapien ornare,
              pharetra est at, efficitur justo. Donec rhoncus lectus sit amet
              tortor blandit, quis convallis velit convallis. Nam finibus cursus
              consequat. Mauris iaculis euismod magna in scelerisque. Nullam
              rutrum aliquam nisl vel posuere. Vivamus sed nisl in mi tristique
              consectetur quis ac mi. Nulla facilisi. Maecenas risus orci,
              facilisis id tempus eu, condimentum vitae ex. Orci varius natoque
              penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              Etiam arcu lacus, interdum egestas leo ut, mattis suscipit lectus.
            </p>
            <p>
              Donec vitae lobortis arcu. Aenean arcu ante, porttitor a dui quis,
              tincidunt viverra eros. Suspendisse finibus elementum nulla, vitae
              mollis ligula placerat in. Nunc rhoncus massa sed dolor blandit
              scelerisque. Ut a leo orci. Curabitur porttitor faucibus ornare.
              Integer efficitur molestie lacus, nec pharetra massa fermentum
              quis. Phasellus malesuada euismod porta. Praesent eget sem ornare,
              ultricies risus nec, elementum turpis. Sed maximus felis sem,
              imperdiet pretium ipsum feugiat vitae.
            </p>
            <p>
              Ut ac congue purus. Praesent erat urna, maximus in lectus quis,
              tincidunt blandit nulla. Nullam ante nunc, laoreet ac odio in,
              molestie pulvinar eros. Cras mollis, est ut dictum condimentum,
              ipsum est rhoncus dolor, malesuada finibus urna magna vel neque.
              Morbi faucibus erat mauris. Nullam lectus nisl, accumsan vitae
              tortor eu, hendrerit pretium arcu. Integer nec ligula ac massa
              semper feugiat eu eu justo. Nulla in mi non ligula laoreet
              ultrices eu vel arcu. Integer auctor, tortor sit amet vestibulum
              pulvinar, neque sem semper odio, sagittis finibus dolor augue
              dictum ligula. Aliquam sit amet imperdiet est. Vestibulum
              fringilla, felis et dignissim iaculis, nulla metus vestibulum
              dolor, non lacinia erat magna ac lacus. Sed eget sapien ut libero
              consequat pharetra. Maecenas at dui fermentum, sagittis nisl sit
              amet, pellentesque mauris. Nunc laoreet consequat ornare. Vivamus
              massa enim, vulputate id tincidunt a, faucibus ut ligula. Sed
              placerat lacus posuere faucibus tincidunt.
            </p>
            <p>
              Proin sed diam non ante tempus congue. Duis eget est sit amet orci
              convallis auctor. Phasellus at sem at sapien aliquet elementum in
              vitae nibh. Ut tristique, dolor nec cursus maximus, ex turpis
              tempus sem, et iaculis dolor odio sit amet metus. Proin tristique
              magna a nunc tincidunt tempus. Curabitur eget ante vitae magna
              blandit efficitur. Sed ut condimentum sem. Integer et varius nisl.
              Sed hendrerit arcu vel dui aliquam, ut egestas velit pharetra.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
export default AboutUs;
