import React, { useState, useContext } from "react";
import { BooksContext } from "../../components/context";
import { Alert } from "../../components/Alert";

const Contact = () => {
  const [contact, setContact] = useState({
    firstName: "",
    email: "",
    number: "",
    msg: "",
  });
  const { alertStuff, handleAlertState } = useContext(BooksContext);
  const [alertState] = alertStuff;

  const handleChange = (e) => {
    const id = e.target.id;
    const value = e.target.value;
    setContact({ ...contact, [id]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    handleAlertState();
    setContact({
      firstName: "",
      email: "",
      number: "",
      msg: "",
    });
  };

  return (
    <div className="pages__wrapper">
      <div className="contact-us">
        {alertState && <Alert text="The form has been sent" />}
        <div className="contact-us__headings">
          <h2>Contact form</h2>
          <h3>Fill the form and expect a call or an email within 48 hours!</h3>
        </div>
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="first-name">*First name:</label>
            <input
              id="firstName"
              type="text"
              value={contact.name}
              onChange={handleChange}
              required
            ></input>
          </div>
          <div>
            <label htmlFor="email">*Your e-mail:</label>
            <input
              id="email"
              type="email"
              value={contact.email}
              onChange={handleChange}
              required
            ></input>
          </div>
          <div>
            <label htmlFor="number">Your phone number:</label>
            <input
              id="number"
              type="number"
              value={contact.number}
              onChange={handleChange}
            ></input>
          </div>
          <div>
            <label htmlFor="message">*Your message:</label>
            <textarea
              id="msg"
              value={contact.msg}
              onChange={handleChange}
              required
            ></textarea>
          </div>
          <div>
            <button type="submit">Send</button>
          </div>
        </form>
      </div>
    </div>
  );
};
export default Contact;
