import { questionsData } from "../../components/data";
import { nanoid } from "nanoid";
import FaqItem from "./item";

const FAQ = () => {

  return (
    <div className="pages__wrapper">
      <div className="faq">
        <h2>Frequently Asked Questions</h2>
        <div className="faq-content">
          {questionsData.map((el) => {
            let id = nanoid();
            return (
              <FaqItem key={id} id={id} el={el}/>         
              );
          })}
        </div>
      </div>
    </div>
  );
};
export default FAQ;
