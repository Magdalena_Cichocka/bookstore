import { useState } from "react";

const FaqItem = ({ el, id }) => {
  const [isOpen, setIsOpen] = useState(false);

  const handleOpen = () => {
    setIsOpen(!isOpen);
  };
  return (
    <>
      {isOpen ? (
        <ActiveReturn handleOpen={handleOpen} el={el} id={id} />
      ) : (
        <InactiveReturn handleOpen={handleOpen} el={el} id={id} />
      )}
    </>
  );
};
const InactiveReturn = ({ handleOpen, el, id }) => {
  return (
    <div className="faq-content__item">
      <div>
        <input
          type="image"
          src="https://www.flaticon.com/svg/static/icons/svg/32/32354.svg"
          alt="open book icon"
          id={id}
          onClick={() => {
            handleOpen();
          }}
        ></input>
        <h3>
          <label htmlFor={id}>{el.question}</label>
        </h3>
      </div>
    </div>
  );
};

const ActiveReturn = ({ handleOpen, el, id }) => {
  return (
    <div className="faq-content__item active">
      <div>
        <input
          className="active"
          type="image"
          src="https://www.flaticon.com/svg/static/icons/svg/43/43212.svg"
          alt="open book icon"
          id={id}
          onClick={() => {
            handleOpen();
          }}
        ></input>
        <h3>
          <label htmlFor={id}>{el.question}</label>
        </h3>
      </div>
      <p>{el.answer}</p>
    </div>
  );
};

export default FaqItem;
