import { BooksContext } from "../../components/context";
import { useContext, useState } from "react";
import CartItem from "../../components/Cart Item";
import { Alert } from "../../components/Alert";

const Cart = () => {
  const { cartStuff, alertStuff, handleAlertState } = useContext(BooksContext);
  const [cartContent] = cartStuff;
  const [alertState] = alertStuff;

  const calcTotal = () => {
    let total = 0;
    cartContent.forEach((el) => (total += el.price * el.amount));
    return total;
  };
  const [total, setTotal] = useState(calcTotal());

  const handleBuy = () => {
    handleAlertState();
  };

  const updateTotal = () => {
    setTotal(calcTotal());
  };

  let totalCost = 0;
  for (let i in cartContent) {
    totalCost += cartContent[i].amount * cartContent[i].price;
  }

  if (cartContent.length > 0) {
    return (
      <div className="pages__wrapper">
        <div className="cart">
          {alertState && <Alert text='Great Choice! Thank you!'/>}
          <div>
            <table>
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Price per unit</th>
                  <th>Amount</th>
                  <th>Price total</th>
                  <th>Change amount</th>
                  <th>Remove</th>
                </tr>
              </thead>
              <tbody>
                {cartContent.map((el) => {
                  let id = el.isbn;
                  return (
                    <tr key={id}>
                      <CartItem el={el} updateTotal={updateTotal} />
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <p>Total: ${totalCost.toFixed(2)}</p>
          </div>
        </div>
        <div className="cart__check-out">
          <button onClick={handleBuy}>Check out</button>
        </div>
      </div>
    );
  } else {
    return (
      <div className="pages__wrapper">
        <div className="cart">
          <p className="empty">The cart is empty.</p>
        </div>
      </div>
    );
  }
};
export default Cart;
