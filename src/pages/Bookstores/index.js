import Bookstore from "../../components/Bookstore";
import {bookstores} from "../../components/data";

const Bookstores = () => {
  return (
    <div className="pages__wrapper">
      <div className="our-bookstores">
        <div className="our-bookstores__headings">
          <h2>You can visit us in person!</h2>
          <h3>Just come to one of the addresses provided below.</h3>
        </div>
        <div className='our-bookstores__places'>
        {bookstores.map((el) => {
          return <Bookstore el={el} />;
        })}</div>
      </div>
    </div>
  );
};
export default Bookstores;
