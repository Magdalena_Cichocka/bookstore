import Filters from "../../components/Filters";
import SearchBar from "../../components/Search Bar";
import SearchResults from "../../components/Search Results";

const Homepage = () => {
  return (
    <div className="pages__wrapper">
      <div className="main-section">
        <Filters />
        <div className="main-section__content">
          <SearchBar />
          <SearchResults />
        </div>
      </div>
    </div>
  );
};
export default Homepage;
