import { useState, useContext } from "react";
import { BooksContext } from "../context";

const CartItem = ({ el, updateTotal }) => {
  const { cartStuff } = useContext(BooksContext);
  const [cartContent, setCartContent] = cartStuff;
  const [editedAmount, setEditedAmount] = useState(el.amount);
  const [amount, setAmount] = useState(el.amount);

  const handleChange = (e) => {
    setEditedAmount(e.target.value);
  };

  const updateAmount = () => {
    if (editedAmount < 1) {
      handleRemove(el);
    } else {
      el.amount = parseInt(editedAmount);
      setAmount(el.amount);
      updateTotal();
    }
  };

  const handleRemove = (el) => {
    const newArray = cartContent.filter((book) => book.isbn !== el.isbn);
    setCartContent(newArray);
    setAmount(el.amount);
    updateTotal();
  };

  const itemPriceTotal = amount * el.price;

  return (
    <>
      <td>{el.title}</td>
      <td>${el.price.toFixed(2)}</td>
      <td id="amount">{amount}</td>
      <td>${itemPriceTotal.toFixed(2)}</td>
      <td>
        <input
          type="number"
          min='0'
          onChange={handleChange}
          value={editedAmount}
        ></input>
        <button
          onClick={() => {
            updateAmount();
          }}
        >
          Change
        </button>
      </td>
      <td>
        <button
          onClick={() => {
            handleRemove(el);
          }}
        >
          X
        </button>
      </td>
    </>
  );
};

export default CartItem;
