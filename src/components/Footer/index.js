const Footer = () => {
    return (
        <div className="footer">
            No copyright @ Lorem Ipsum 2020, no rights reserved.<br/>Icons are from flaticon.com and images are from pexels.com.
        </div>
    )
}
export default Footer;