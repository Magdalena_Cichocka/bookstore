import { BooksContext } from "../../components/context";
import { useContext, useState, useRef } from "react";
import Book from "../Book";
import Pagination from "../Pagination";

const SearchResults = () => {
  const { bookStuff, bookListRef, pageStuff } = useContext(BooksContext);
  const [bookList] = bookStuff;
  const [currentPage, setCurrentPage] = pageStuff;
  const [booksPerPage] = useState(5);
  const indexOfLastBook = currentPage * booksPerPage;
  const indexOfFirstBook = indexOfLastBook - booksPerPage;
  const currentBooks = bookList.slice(indexOfFirstBook, indexOfLastBook);

  const handlePageChange = (number) => {
    setCurrentPage(number);
    window.scrollTo(0, bookListRef.current.offsetTop);
  };

  return (
    <>
      <div className="tip">
        <div>Click on book cover or title to see the details!</div>
      </div>
      <div className="book-list" ref={bookListRef}>
        {currentBooks.map((el) => {
          const id = el.isbn;
          return <Book el={el} key={id} />;
        })}
      </div>
      <Pagination
        booksPerPage={booksPerPage}
        totalBooks={bookList.length}
        handlePageChange={handlePageChange}
        currentPage={currentPage}
      />
    </>
  );
};
export default SearchResults;
