const Bookstore = ({ el }) => {
  return (
    <div className="bookstore">
      <div className="bookstore-image">
        <img src={el.picture} alt="bookstore"></img>
      </div>
      <div className="bookstore-info">
        <div>
          <h2>{el.town}</h2>
          <h3>{el.street}</h3>
        </div>
        <div>
          <p>phone: {el.phone}</p>
          <p>e-mail: {el.email}</p>
        </div>
        <div>
          <p>Open:</p>
          <p>Mon-Fri: {el.hours.mon}</p>
          <p>Sat: {el.hours.sat}</p>
          <p>Sun: {el.hours.sun}</p>
        </div>
      </div>
    </div>
  );
};
export default Bookstore;
