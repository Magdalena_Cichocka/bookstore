import React, { useState, createContext, useEffect, useRef } from "react";
import { books } from "../data";

export const BooksContext = createContext();

export const BooksProvider = (props) => {
  const getLocalStorage = () => {
    let cart = localStorage.getItem("cart");
    if (cart) {
      return (cart = JSON.parse(localStorage.getItem("cart")));
    } else {
      return [];
    }
  };

  const [cartContent, setCartContent] = useState(getLocalStorage());

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cartContent));
  }, [cartContent]);

  const [bookList, setBookList] = useState(books);
  const [alertState, setAlertState] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);

  const handleTotal = (array) => {
    const prices = [];
    if (cartContent.length > 0) {
      for (let i in array) {
        prices.push(array[i].price);
      }
      const result = prices.reduce((a, b) => a + b).toFixed(2);
      return result;
    } else return 0;
  };

  const handleAlertState = () => {
    setAlertState(true);
    setTimeout(() => {
      setAlertState(false);
    }, 2000);
  };

  const bookListRef = useRef(null);

  return (
    <BooksContext.Provider
      value={{
        bookStuff: [bookList, setBookList],
        cartStuff: [cartContent, setCartContent],
        alertStuff: [alertState, setAlertState],
        handleTotal,
        handleAlertState,
        bookListRef,
        pageStuff:[currentPage, setCurrentPage]
      }}
    >
      {props.children}
    </BooksContext.Provider>
  );
};
