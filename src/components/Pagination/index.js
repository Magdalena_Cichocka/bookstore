const Pagination = ({
  booksPerPage,
  totalBooks,
  handlePageChange,
  currentPage,
}) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalBooks / booksPerPage); i++) {
    pageNumbers.push(i);
  }
  return (
    <div className="pagination">
      {pageNumbers.map((number) => {
        let pageClass = "";
        currentPage === number
          ? (pageClass = "active")
          : (pageClass = "passive");

        return (
          <button
            key={number}
            className={pageClass}
            onClick={() => {
              handlePageChange(number);
            }}
          >
            {number}
          </button>
        );
      })}
    </div>
  );
};
export default Pagination;
