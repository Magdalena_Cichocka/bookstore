import { Link } from "react-router-dom";
import { useContext } from "react";
import { BooksContext } from "../context";

const TopSection = () => {
  const { cartStuff } = useContext(BooksContext);
  const [cartContent] = cartStuff;
  let amountOfItems = 0;
  let totalCost = 0;

  for (let i in cartContent) {
    amountOfItems += parseInt(cartContent[i].amount);
    totalCost += cartContent[i].amount * cartContent[i].price;
  }
  return (
    <>
      <div className="top-section">
        <div className="top-section__logo">
          <Link to="/">The BookStore</Link>
        </div>
        <div className="top-section__links">
          <Link to="/about-us">About us</Link>
          <Link to="/bookstores">Our bookstores</Link>
          <Link to="/contact">Contact us</Link>
          <Link to="/faq">FAQ</Link>
        </div>
        <div className="top-section__cart">
          <Link to="/cart">
            <img
              src="https://www.flaticon.com/svg/static/icons/svg/3662/3662258.svg"
              alt="shopping cart icon"
            />
          </Link>
          <p>
            {amountOfItems} items, ${totalCost.toFixed(2)}
          </p>
        </div>
      </div>
      <div className="header-img">
        <Link to="/">
          <img
            src="https://images.pexels.com/photos/159711/books-bookstore-book-reading-159711.jpeg?cs=srgb&dl=pexels-pixabay-159711.jpg&fm=jpg"
            alt="books"
          />
        </Link>
      </div>
    </>
  );
};
export default TopSection;
