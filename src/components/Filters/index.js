import { books } from "../data";
import { nanoid } from "nanoid";
import FilterButton from '../Filter Button';
import { useContext, useState } from "react";
import { BooksContext } from "../context";

const Filters = () => {
  const buttonsArray = generateButtons();
  const {bookStuff, bookListRef} = useContext(BooksContext);
  const [bookList, setBookList] = bookStuff;
  const [showButtons, setShowButtons] = useState(false);

  const handleShowAll = () => {
    setBookList(books);
    window.scrollTo(0, bookListRef.current.offsetTop);
  }
  if (showButtons) {
  return (
  <div className="filter-buttons">
    <button className='special' type='button' onClick={() => {setShowButtons(!showButtons)}}>Hide filters</button>
        <button type='button' onClick={()=>{handleShowAll()}}>Show all</button>
      {buttonsArray.map((el) => {
        const id = nanoid();
        return (
            <FilterButton value={el} key={id} bookListRef={bookListRef}>
            {el}</FilterButton>
          
        );
      })}
    </div>
  );}
  else {
    return (
      <div className="filter-buttons">
        <button className='special' type='button' onClick={() => {setShowButtons(!showButtons)}}>Show filters</button>
      </div>
    )
  }
};

const generateButtons = () => {
  const categoriesArray = [];
  for (let i = 0; i < books.length; i++) {
    let lowerCase = [];
    for (let j = 0; j< books[i].categories.length; j++) {
      lowerCase.push(books[i].categories[j].toLowerCase());
    }
    categoriesArray.push(...lowerCase);
  }
  const buttonValuesSet = new Set(categoriesArray);
  const buttonValuesArray = [...buttonValuesSet];
  const result = buttonValuesArray.sort();
  return result;
};

export default Filters;
