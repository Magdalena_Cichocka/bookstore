import { useRef, useContext } from "react";
import { BooksContext } from "../context";
import { books } from "../data";

const SearchBar = () => {
  const { bookStuff } = useContext(BooksContext);
  const [bookList, setBookList] = bookStuff;
  const refContainer = useRef(null);

  const handleChange = (e) => {
    const filteredResults = [];
    let regex = new RegExp(refContainer.current.value, "gi");
    for (let i in books) {
      if (
        regex.test(books[i].title) ||
        regex.test(books[i].author) ||
        regex.test(books[i].categories)
      ) {
        filteredResults.push(books[i]);
      }
    }

    setBookList(filteredResults);
  };

  return (
    <div className="search-bar">
      <form>
        <label htmlFor="search-bar">Search by author, title, genre...</label>
        <input
          id="search-bar"
          type="text"
          onChange={handleChange}
          ref={refContainer}
          placeholder="Start typing to filter..."
        ></input>
      </form>
    </div>
  );
};
export default SearchBar;
