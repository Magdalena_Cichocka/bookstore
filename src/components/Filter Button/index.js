import { useContext } from "react";
import { BooksContext } from "../context";
import { books } from "../data";

const FilterButton = ({ value, bookListRef }) => {
  const { bookStuff, pageStuff } = useContext(BooksContext);
  const [bookList, setBookList] = bookStuff;
  const [currentPage, setCurrentPage] = pageStuff;

  const handleFilter = () => {
    const filteredBooks = [];
    for (let i = 0; i < books.length; i++) {
      let arrayOfCat = books[i].categories.map((el) => el.toLowerCase());
      if (arrayOfCat.includes(value)) {
        console.log("has");
        filteredBooks.push(books[i]);
      }
    }
    setBookList(filteredBooks);
    setCurrentPage(1);
    window.scrollTo(0, bookListRef.current.offsetTop);
  };

  return (
    <button
      type="button"
      id={value}
      onClick={() => {
        handleFilter();
      }}
    >
      {value}
    </button>
  );
};
export default FilterButton;
