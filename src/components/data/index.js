// books array partially from https://knowledge.kitchen/Books_example_data_set

export const books = [
  {
    isbn: 9780241375273,
    title: "Pride and Prejudice",
    author: "Austen, Jane",
    year_written: 1814,
    edition: "Penguin",
    price: 18.2,
    pages: 432,
    cover:
      "https://www.penguin.co.uk/content/dam/prh/books/559/55905/9780141040349.jpg.transform/PRHDesktopWide_small/image.jpg",
    categories: ["English literature", "Romance novel"],
    shortDescription:
      "Cras egestas fermentum turpis, in vulputate lacus auctor eget. Fusce consequat vehicula ultrices. Proin non ullamcorper diam.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
  {
    isbn: 9780241265543,
    title: "War and Peace",
    author: "Tolstoy, Leo",
    year_written: 1865,
    edition: "Penguin",
    price: 12.7,
    pages: 1225,
    cover:
      "https://www.penguin.co.uk/content/dam/prh/books/301/301515/9780241265543.jpg.transform/PRHDesktopWide_small/image.jpg",
    categories: ["Russian literature", "Historical novel"],
    shortDescription:
      "Suspendisse interdum sit amet diam vitae lobortis. Proin sit amet lacus imperdiet elit ullamcorper eleifend vitae eu enim. Phasellus porttitor porttitor tempus.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
  {
    isbn: 9780141199610,
    title: "Anna Karenina",
    author: "Tolstoy, Leo",
    year_written: 1875,
    edition: "Penguin",
    price: 13.5,
    pages: 864,
    cover:
      "https://www.penguin.co.uk/content/dam/prh/books/354/35473/9780141199610.jpg.transform/PRHDesktopWide_small/image.jpg",
    categories: ["Russian literature", "Realist novel"],
    shortDescription:
      "Aenean in nulla ac augue efficitur porttitor eget non lorem. Curabitur molestie consequat turpis, blandit gravida erat varius ut. Mauris blandit urna id lectus fermentum hendrerit.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
  {
    isbn: 9780241956793,
    title: "Mrs. Dalloway",
    author: "Woolf, Virginia",
    year_written: 1925,
    edition: "Harcourt Brace",
    price: 25,
    pages: 208,
    cover:
      "https://www.penguin.co.uk/content/dam/prh/books/251/25156/9780241956793.jpg.transform/PRHDesktopWide_small/image.jpg",
    categories: ["English literature", "Historical novel"],
    shortDescription:
      "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Integer id urna sed nisi rhoncus interdum. Etiam ac est efficitur, aliquet elit id, iaculis orci.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
  {
    isbn: 9781841150345,
    title: "The Hours",
    author: "Cunnningham, Michael",
    year_written: 1999,
    edition: "Harcourt Brace",
    price: 12.35,
    pages: 230,
    cover:
      "https://upload.wikimedia.org/wikipedia/en/8/8d/The_Hours_novella.jpg",
    categories: ["American literature"],
    shortDescription:
      "Sed non massa laoreet, volutpat sapien a, pretium ligula. Fusce accumsan tempus blandit. Fusce eu orci aliquet, viverra libero a, bibendum arcu.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
  {
    isbn: 9780141974262,
    title: "Bleak House",
    author: "Dickens, Charles",
    year_written: 1870,
    edition: "Random House",
    price: 5.75,
    pages: 1104,
    cover:
      "https://www.penguin.co.uk/content/dam/prh/books/183/183712/9780141974262.jpg.transform/PRHDesktopWide_small/image.jpg",
    categories: ["English literature"],
    shortDescription:
      "Praesent ut nulla at nisl ultrices mattis. Nunc eget accumsan massa, ac facilisis justo. Pellentesque lacinia vulputate ultrices. Maecenas interdum imperdiet massa a hendrerit. Praesent ac elit odio.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
  {
    isbn: 9780141321103,
    title: "Tom Sawyer",
    author: "Twain, Mark",
    year_written: 1862,
    edition: "Random House",
    price: 7.75,
    pages: 64,
    cover:
      "https://www.penguin.co.uk/content/dam/prh/books/315/315885/9780241430880.jpg.transform/PRHDesktopWide_small/image.jpg",
    categories: ["Children's literature", "American literature"],
    shortDescription:
      "Nulla rhoncus porttitor lacus, feugiat sollicitudin tellus convallis a. In varius a odio eu placerat. Pellentesque at tempus dui. Morbi pharetra laoreet facilisis.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
  {
    isbn: 9781784874476,
    title: "A Room of One's Own",
    author: "Woolf, Virginia",
    year_written: 1922,
    edition: "Penguin",
    price: 29,
    pages: 172,
    cover: "https://upload.wikimedia.org/wikipedia/en/3/31/ARoomOfOnesOwn.jpg",
    categories: ["English literature"],
    shortDescription:
      "Proin lacus dolor, pellentesque non quam id, mattis fermentum sapien. Duis ac sapien sagittis, laoreet nisi accumsan, commodo turpis.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
  {
    isbn: 9780545010221,
    title: "Harry Potter and the Deathly Hallows",
    author: "Rowling, J.K.",
    year_written: 2000,
    edition: "Harcourt Brace",
    price: 19.95,
    pages: 607,
    cover:
      "https://upload.wikimedia.org/wikipedia/en/a/a9/Harry_Potter_and_the_Deathly_Hallows.jpg",
    categories: ["Fantasy", "American literature"],
    shortDescription:
      "Cras ex libero, euismod quis dolor et, eleifend ultrices felis. Proin maximus nisi efficitur eros posuere elementum.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
  {
    isbn: 9780241971826,
    title: "One Hundred Years of Solitude",
    author: "Marquez, Gabriel Garcia",
    year_written: 1967,
    edition: "Harper  Perennial",
    price: 14.0,
    pages: 239,
    cover:
      "https://upload.wikimedia.org/wikipedia/en/thumb/a/a0/Cien_a%C3%B1os_de_soledad_%28book_cover%2C_1967%29.jpg/220px-Cien_a%C3%B1os_de_soledad_%28book_cover%2C_1967%29.jpg",
    categories: ["Colombian literature"],
    shortDescription:
      "Suspendisse ac metus porta, aliquam velit ut, tempus risus. Suspendisse eget interdum elit. Curabitur tempus quam magna, vitae mattis tortor tristique sit amet. Nullam scelerisque consequat leo sed maximus.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
  {
    isbn: 9780261103252,
    title: "Lord of the Rings",
    author: "Tolkien, J.R.",
    year_written: 1937,
    edition: "Penguin",
    price: 27.45,
    pages: 479,
    cover:
      "https://upload.wikimedia.org/wikipedia/en/thumb/e/e9/First_Single_Volume_Edition_of_The_Lord_of_the_Rings.gif/220px-First_Single_Volume_Edition_of_The_Lord_of_the_Rings.gif",
    categories: ["Fantasy", "English literature"],
    shortDescription:
      "Pellentesque vehicula commodo feugiat. Integer in blandit enim. Vestibulum lacus ligula, eleifend sed diam vel, convallis finibus nisl.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
  {
    isbn: 9781519727497,
    title: "The Knights of the Cross",
    author: "Sienkiewicz, Henryk",
    year_written: 1900,
    edition: "Wł. Anczyc & Co. Press",
    price: 27.45,
    pages: 295,
    cover:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/PL_Henryk_Sienkiewicz-Krzy%C5%BCacy_0005.jpeg/220px-PL_Henryk_Sienkiewicz-Krzy%C5%BCacy_0005.jpeg",
    categories: ["Historical novel", "Polish literature"],
    shortDescription:
      "Mauris eget tincidunt ex. Sed eu neque in massa fringilla tempor vitae id libero. Donec a mi varius, tincidunt orci nec, pulvinar nisl.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
  {
    isbn: 9781784871970,
    title: "Crime and Punishment",
    author: "Dostoyevsky, Fyodor",
    year_written: 1866,
    edition: "The Russian Messenger",
    price: 27.45,
    pages: 287,
    cover:
      "https://upload.wikimedia.org/wikipedia/en/thumb/4/4b/Crimeandpunishmentcover.png/220px-Crimeandpunishmentcover.png",
    categories: ["Russian literature", "Psychological fiction"],
    shortDescription:
      "Donec dictum eros lobortis ex molestie, eget posuere urna sollicitudin. Aenean iaculis ullamcorper odio in consequat. Duis egestas augue ac lorem varius, id mollis dolor pulvinar.",
    longDescription:
      "Sed quis ligula vitae dui vehicula ultricies ac lobortis est. Nam in malesuada justo, et lobortis ligula. Ut varius, massa in mollis vestibulum, nulla nibh tincidunt massa, vel aliquet dui tortor id orci. Fusce nec condimentum nisi, ut mattis arcu. Sed urna lacus, dignissim in odio eu, suscipit scelerisque velit. Integer dolor nisi, tincidunt a nulla id, tempus malesuada quam. Vivamus vel sem dictum, rhoncus est a, placerat dolor. Sed nec rhoncus dolor. Donec id ipsum non nunc tempus ultrices. Maecenas augue justo, posuere at sagittis volutpat, pharetra ut nisi. Praesent auctor non mauris sodales faucibus. Sed nec risus mollis, fermentum leo convallis, cursus ligula. Praesent ut ligula at nisl ornare blandit. Curabitur a lacinia ex. Suspendisse semper dolor lacus, quis tempor urna suscipit in. Fusce at augue non mi pharetra blandit sit amet nec nisl.  Cras consectetur tincidunt nulla. Vivamus auctor tristique ligula. Praesent eu aliquam nisl. Sed laoreet feugiat vulputate. Cras ac tincidunt arcu. Nulla est enim, lacinia non mi ut, maximus euismod ipsum. Sed metus massa, tincidunt quis placerat quis, fermentum a diam. Nulla vitae dui efficitur neque euismod varius molestie at metus. Nunc maximus felis condimentum malesuada blandit. Vivamus turpis sapien, rutrum at dolor non, porttitor ultrices nulla. Cras lectus lacus, euismod id ex id, pellentesque congue magna. Suspendisse et quam eget neque ornare dignissim id eget nunc. Nullam mi nunc, condimentum eget metus sit amet, accumsan dictum quam.",
  },
];

export const questionsData = [
  {
    question: "Are your books environment-friendly?",
    answer:
      "Proin rutrum sed lacus a pellentesque. Nulla convallis, tellus nec venenatis condimentum, nisi ipsum vehicula libero, sed tincidunt justo nibh eu dui. Proin sed augue ut augue lacinia congue tempor eget justo. Etiam ut ante suscipit, imperdiet magna vel, auctor justo.",
  },
  {
    question:
      "Are the prices of books online same as the prices in physical stores?",
    answer:
      "Praesent semper mi id rhoncus ullamcorper. Proin eu vestibulum neque, non pulvinar purus. Nam eget placerat libero. Mauris non dui quis lorem aliquet pharetra.",
  },
  {
    question: "Do you use toilet paper in your recycled paper?",
    answer:
      "Nunc efficitur efficitur tortor sit amet ultrices. Ut tincidunt tortor quis orci lacinia auctor a eget lorem. Suspendisse auctor efficitur nisl, non cursus ante consequat nec. In aliquam pharetra tellus, eu semper odio volutpat a. Phasellus ac ultricies nisl, eu vehicula sapien.",
  },
  {
    question: "Is it safe for my pet to chew your books?",
    answer:
      "Cras placerat pretium augue, at rhoncus odio euismod ut. Duis mattis quis purus blandit ornare. Aenean lacinia, risus vel lobortis bibendum, libero ante blandit diam, vel consectetur neque orci aliquam ligula.",
  },
  {
    question: "How much is the shipping? Why is it so much?!",
    answer:
      "In condimentum libero arcu, sed accumsan turpis malesuada ac. Aenean molestie odio tellus, sed porta ligula ultricies quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In ac quam hendrerit, commodo felis vel, imperdiet est.",
  },
  {
    question: "Where is the author's sense of humour?",
    answer:
      "It's never been here to begin with. Nam et tincidunt augue, quis molestie nibh. Nam pretium volutpat rhoncus. Sed nec accumsan massa, at tristique est. Mauris imperdiet aliquet nisi, at imperdiet velit tincidunt vitae. Cras imperdiet viverra elit eget lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. ",
  },
];

export const bookstores = [
  {
    picture:
      "https://images.pexels.com/photos/1850021/pexels-photo-1850021.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    street: "ul. Naleśników z Dżemem",
    town: "Iława",
    phone: "654 321 987",
    email: "ilawa@greatbooks.com",
    hours: {mon: "10:00-20:00", sat: "9:00-14:00", sun: "Closed"},
  },
  {
    picture:
      "https://images.pexels.com/photos/3646172/pexels-photo-3646172.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    street: "ul. Miaukowa",
    town: "Toruń",
    phone: "123 456 789",
    email: "torun@greatbooks.com",
    hours: {mon: "9:00-19:00", sat: "10:00-16:00", sun: "Closed"},
  },
  {
    picture:
      "https://images.pexels.com/photos/3494806/pexels-photo-3494806.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    street: "ul. Herbatnikowa",
    town: "Włocławek",
    phone: "456 789 123",
    email: "wloclawek@greatbooks.com",
    hours: {mon: "8:00-20:00", sat: "10:00-15:00", sun: "Closed"},
  },
  {
    picture:
      "https://images.pexels.com/photos/1261180/pexels-photo-1261180.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    street: "ul. Orzechów Włoskich",
    town: "Olsztyn",
    phone: "987 654 321",
    email: "olsztyn@greatbooks.com",
    hours: {mon: "10:00-19:00", sat: "10:00-14:00", sun: "Closed"},
  },
];
