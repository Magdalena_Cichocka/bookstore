import { useContext, useState } from "react";
import { BooksContext } from "../context";
import {Alert} from "../Alert";

const BookInput = ({ el }) => {
  const { cartStuff, alertStuff, handleAlertState } = useContext(BooksContext);
  const [cartContent, setCartContent] = cartStuff;
  const [alertState] = alertStuff;
  const [inputState, setInputState] = useState(1);

  const handleBuyItem = (e, el) => {
    e.preventDefault();

    const isbns = [];
    for (let i in cartContent) {
      isbns.push(cartContent[i].isbn);
    }
    let amount = parseInt(inputState);
    if (!isbns.includes(el.isbn)) {
      let newEl = { ...el, amount };
      const newArray = [...cartContent, newEl];
      setCartContent(newArray);
      setInputState(1);
      handleAlertState();
    } else {
      for (let i in cartContent) {
        if (cartContent[i].isbn === el.isbn) {
          cartContent[i].amount += amount;
        }
        setInputState(1);
        handleAlertState();
      }
    }
  };
  const handleAmountChange = (e) => {
    setInputState(e.target.value);
  };

  return (
    <div className='book-input'>
        {alertState && <Alert text={'Added to the cart'}/>}
          <form>
            <input
              type="number"
              placeholder={1}
              value={inputState}
              min='1'
              onChange={(e) => {
                handleAmountChange(e);
              }}
            ></input>
            <button
              onClick={(e) => {
                handleBuyItem(e, el);
              }}
            >
              Buy
            </button>
          </form>
    </div>
  );
};
export default BookInput;
