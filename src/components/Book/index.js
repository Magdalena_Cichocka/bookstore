import { Link } from "react-router-dom";
import BookInput from '../../components/Book Input';

const Book = ({ el }) => {

  return (
    <>
      <div className="book">
        <div className="book__image">
          <Link to={`book/${el.isbn}`}>
            <img src={el.cover} alt={`${el.title} cover`}></img>
          </Link>
          <BookInput el={el}/>
        </div>
        <div className="book__data">
          <div className="book__data__price">{`$${el.price.toFixed(2)}`}</div>
          <div>
            <h2>
              <Link to={`book/${el.isbn}`}>{el.title}</Link>
            </h2>
          </div>
          <div>
            <h3>{el.author}</h3>
          </div>
          <p>{el.shortDescription}</p>
          <p className='book__data__categories'>{el.categories.map((el)=>`${el} `)}</p>
        </div>
      </div>
    </>
  );
};
export default Book;
