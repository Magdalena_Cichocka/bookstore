export const Alert = ({ text }) => {
  return <div className="alert">{text}</div>;
};
